from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.contrib import messages
from django.db import IntegrityError
from django.contrib.auth.hashers import make_password, check_password

from .forms import RegisterForm, LoginForm, AdminRegisterForm
from .models import User
from helpers.common import create_user_session
from books.models import Book, Order

# Create your views here.
class RegisterView(TemplateView):
    template_name = 'user/register.html'

    def get(self, request):
        if 'userauth' in request.session:
            role = request.session.get('userauth', {}).get('role')
            if role == 1:
                return redirect('user-dashboard')
            else:
                return redirect('user-list')
        return render(request, self.template_name, {} )
    
    def post(self, request):
        data = {
            'name': request.POST.get('name'),
            'email': request.POST.get('email'),
            'password': make_password(request.POST.get('password')),
            'mobile': request.POST.get('mobile'),
            'role': 1
        }
        try:
            userObj = User(**data)
            userObj.save()
            messages.success(request, 'User Registered successfully !!')
            return redirect('login')
        except IntegrityError as e:
            messages.error(request, "This email is already exists Please try with another email")

        return render(request, self.template_name, {} )


class LoginView(TemplateView):
    template_name = 'user/login.html'
    form = LoginForm

    def get(self, request):
        if 'userauth' in request.session:
            role = request.session.get('userauth', {}).get('role')
            if role == 1:
                return redirect('user-dashboard')
            else:
                return redirect('user-list')

        return render(request, self.template_name, {'form': self.form()} )

    def post(self, request):
        if 'userauth' in request.session:
            return redirect('user-list')

        form = self.form(request.POST)

        if form.is_valid():
            try:
                user_email = request.POST.get('email')
                user_password = request.POST.get('password')
                user_obj = User.objects.get(email=user_email)
                # print(user_obj)
                if check_password(user_password, user_obj.password):
                    create_user_session(request, user_obj)
                    if user_obj.role == 2:
                        return redirect('user-list')
                    elif user_obj.role == 1:
                        return redirect('user-dashboard')
                else:
                    messages.error(request, "Invalid password.")
            except Exception as e:
                messages.error(request, str(e))

        return render(request, self.template_name, {'form': form})


class Logout(TemplateView):
    def get(self, request):
        role = request.session.get('userauth', {}).get('role')
        del request.session['userauth']
        messages.success(request, 'Logged out successfully')
        return redirect('login')


class AdminRegisterView(TemplateView):
    template_name = 'user/admin_register.html'

    def get(self, request):
        if 'userauth' in request.session:
            role = request.session.get('userauth', {}).get('role')
            if role == 1:
                return redirect('user-dashboard')
            else:
                return redirect('user-list')
        return render(request, self.template_name, {})

    def post(self, request):
        form = self.form(request.POST)

        if form.is_valid():
            data = {
                'name': request.POST.get('name'),
                'email': request.POST.get('email'),
                'password': make_password(request.POST.get('password')),
                'mobile': request.POST.get('mobile'),
                'role': 2
            }
            try:
                userObj = User(**data)
                userObj.save()
                messages.success(request, 'Admin Registered successfully !!')
                return redirect('login')
            except IntegrityError as e:
                messages.error(request, "This email is already exists Please try with another email")

        return render(request, self.template_name, {})


def user_dashboard(request):
    book_lists = Book.objects.all().order_by('-created_on')
    return render(request, 'user/user-dashboard.html', {'books': book_lists})


def apply_book(request, book_id):
    try:
        data = {
            'user_id': request.session.get('userauth', {}).get('user_id'),
            'book_id': int(book_id)
        }
        Order(**data).save()
        messages.success(request, 'Successfully apply, waiting for admin approval..')
    except Exception as e:
        messages.error(request, str(e))
    return redirect('user-dashboard')


def book_orders(request):
    user_id = request.session.get('userauth', {}).get('user_id')
    order_lists = Order.objects.select_related('book').filter(user_id=user_id).order_by('-created_on')
    return render(request, 'user/book-orders.html', {'orders': order_lists})