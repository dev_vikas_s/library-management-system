from django.conf import settings

def currentUserName(request):
	return request.session.get('userauth', {}).get('name')


def currentUserRole(request):
	return  request.session.get('userauth', {}).get('role')

def getRoleName(request):
	role_id = request.session.get('userauth', {}).get('role')
	if role_id == 1: 
		role_name = 'User'
	elif role_id == 2:
		role_name = 'Admin'
	else:
		role_name = ''
	
	return role_name

def globalTemplateVar(request):
	return {
		'USER_ROLE': currentUserRole(request),
		'USER_NAME': currentUserName(request),
		'ROLE_NAME': getRoleName(request),
	}
