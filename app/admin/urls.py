from django.urls import path
from . import views
from django.conf.urls import url

urlpatterns = [
    url(r'^user-list/$', views.UserListView.as_view(), name='user-list'),
    url(r'^create-user/$', views.CreateUserView.as_view(), name='create-user'),
    url(r'^delete/(?P<user_id>[0-9]+)$', views.user_delete, name="user-delete"),
    url(r'^send-fine-user-mail/(?P<user_id>[0-9]+)/(?P<amount>[0-9]+)$', views.send_fine_user_mail, name="send-fine-mail"),
    url(r'^book-request/$', views.book_request, name="book-request"),
    url(r'^change-order-status/(?P<order_id>[0-9]+)/(?P<status>[1-3]+)$', views.change_order_status, name="change-order-status"),
]