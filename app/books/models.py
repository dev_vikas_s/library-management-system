from django.db import models
from django.utils import timezone
from user.models import User


class Book(models.Model):
    isbn = models.CharField(max_length=100, unique=True)
    category = models.CharField(max_length=50, blank=False, null=False, default=None)
    name = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    publisher = models.CharField(max_length=128, blank=False, null=False, default=None)
    price = models.DecimalField(max_digits=5,decimal_places=2)
    quantity = models.IntegerField(blank=True, null=True)
    created_on = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'books'

class Order(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    book = models.ForeignKey(Book,on_delete=models.CASCADE)
    status = models.IntegerField(blank=False, null=False, default=1, help_text='1->Sent, 2->Issued, 3->Reject, ', choices=((1, 'Sent'), (2, 'Issued'), (3, 'Reject')))
    created_on = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.user

    class Meta:
        db_table = 'order'
