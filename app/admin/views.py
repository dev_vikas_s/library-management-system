from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.contrib import messages
from django.contrib.auth.hashers import make_password, check_password
from django.db import IntegrityError
from user.models import User
from books.models import Order
from helpers.common import send_fine_mail



class UserListView(TemplateView):
    def get(self, request):
        users_list = User.objects.filter(role=1).order_by('-created_on')
        return render(request, 'admin/user_list.html', {'users': users_list} )


class CreateUserView(TemplateView):
    def get(self, request):
        return render(request, 'admin/user_create.html', {} )
    
    def post(self, request):
        data = {
            'name': request.POST.get('name'),
            'email': request.POST.get('email'),
            'password': make_password(request.POST.get('password')),
            'mobile': request.POST.get('mobile'),
            'role': 1
        }
        try:
            userObj = User(**data)
            userObj.save()
            messages.success(request, 'User Registered successfully !!')
            return redirect('user-list')
        except IntegrityError as e:
            messages.error(request, "This email is already exists Please try with another email")
        
        return render(request, 'admin/user_create.html', {} )




def user_delete(request, user_id):
    try:
        User.objects.filter(id=user_id).delete()
        messages.success(request, 'User deleted successfully !!')
    except Exception as e:
        messages.error(request, "Error: "+str(e))

    return redirect('user-list')


def book_request(request):
    order_list = Order.objects.select_related('book', 'user').all().order_by('-created_on')
    return render(request, 'admin/book-request.html', {'orders': order_list})


def change_order_status(request, order_id, status):
    try:
        status = int(status)

        Order.objects.filter(id=order_id).update(status=status)
        messages.success(request, 'Status Update successfully !!')
    except Exception as e:
        messages.error(request, str(e))

    return redirect('book-request')


def send_fine_user_mail(request, user_id, amount):
    try:
        user_obj = User.objects.get(id=user_id)
        name = user_obj.name
        email = user_obj.email
        # email = "vshri119@gmail.com"

        send_fine_mail(name, email, amount)
        messages.success(request, 'Mail sent to user !!')
    except Exception as e:
        messages.error(request, str(e))

    return redirect('book-request')
