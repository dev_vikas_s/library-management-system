from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.contrib import messages
from .models import Book
from helpers.common import get_books_category

# Create your views here.

def index(request):
    books = Book.objects.all().order_by('-created_on')
    return render(request, 'books/index.html', {'books': books, 'get_books_category': get_books_category()})


def add(request):
    if request.method == 'POST':
        data = {
            'isbn':request.POST.get('b_id'),
            'name': request.POST.get('name'),
            'category': request.POST.get('category'),
            'author': request.POST.get('author'),
            'publisher': request.POST.get('publisher'),
            'price': int(request.POST.get('price')),
            'quantity': int(request.POST.get('quantity'))
        }
        try:
            userObj = Book(**data).save()
            messages.success(request, 'Book added successfully !!')
            return redirect('books-list')
        except Exception as e:
            messages.error(request, str(e))
    return render(request, 'books/add.html', {'get_books_category': get_books_category()})


def delete(request, book_id):
    try:
        Book.objects.filter(id=book_id).delete()
        messages.success(request, 'Book deleted successfully !!')
    except Exception as e:
        messages.error(request, "Error: "+str(e))

    return redirect('books-list')


def book_details(request, book_id):
    try:
        details = Book.objects.get(id=book_id)
    except Exception as e:
        messages.error(request, str(e))
        return redirect('user-dashboard')

    return render(request, 'user/action_view.html', {'book': details})
