from django import template
from datetime import datetime

register = template.Library()

@register.filter(name='timediff')
def timediff(datestring):
    format = "%Y-%m-%d %H:%M:%S"

    datestring = datetime.strptime(str(datestring)[:19], format)
    now = datetime.now().strftime(format)
    cur_time = datetime.strptime(now, format)
    
    diff = cur_time - datestring
    days = int(diff.days)
    return days
