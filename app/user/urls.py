from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^login', views.LoginView.as_view(), name='login'), # Notice the URL has been named
    url(r'^register/$', views.RegisterView.as_view(), name='register'),
    url(r'^admin_register/$', views.AdminRegisterView.as_view(), name='admin_register'),
    url(r'^logout/$', views.Logout.as_view(), name='logout'),
    url(r'^user-dashboard/$', views.user_dashboard, name='user-dashboard'),
    url(r'^apply-book/(?P<book_id>[0-9]+)$', views.apply_book, name='apply_book'),
    url(r'^book-orders/$', views.book_orders, name='book-orders'),
]