def create_user_session(request, user_obj):
    request.session['userauth'] = {
        'name': user_obj.name,
        'email': user_obj.email,
        'user_id': user_obj.id,
        'role': user_obj.role
    }
    return True


def get_books_category():
    return {
        'science': 'Science',
        'fantasy': 'Fantasy',
        'thriller': 'Thriller',
        'romance': 'Romance'
    }

''' Send Mail Function '''
def send_email(email, subject, htmlmessage, langcode=None, heading=None):
    try :          
        template = htmlmessage
        # send email

        try :
            from django.core.mail import send_mail
            send_mail(subject, '', 'info@rentlease.com',[email], fail_silently=False,auth_user=None, auth_password=None, connection=None,html_message=template)
        except Exception as e:
            print(e)
            return True
    except Exception as e:
        print(e)
        pass


# use for send register mail
def send_fine_mail(name, email, amount):
    import threading

    try :
        content = "Hi ##USER##,<br/> Your Late charges Rs. ##AMOUNT## due on Rent and Lease."
        # content = "Hi ##USER##,<br/> Congrats for create account with FinTDme.<br/> Please verify your account on click given link <a href='"+str(url)+"'>Click to Verify</a>"

        subject = "Alert mail"
        heading = "verification mail"

        content = content.replace("##AMOUNT##", str(amount))
        content = content.replace("##USER##", name)

        threading.Thread(target=send_email,args=(email, subject, content)).start()        
    except Exception as e:
        print(e)
        pass
    return 