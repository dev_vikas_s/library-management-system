# Generated by Django 2.0.2 on 2019-03-30 16:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0006_auto_20190330_2206'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='book',
            name='book_id',
        ),
    ]
